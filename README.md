---
title: Snake Game
---

# Snake Game in Rust

A little game just to work a little with Rust proggraming language and some of
the available 2D graphics libraries.

I followed [this git](https://gist.github.com/AndrewJakubowicz/9972b5d46be474c186a2dc3a71326de4)

## Gameplay

Use the arrow keys to move the red square, the main point is to catch the
black dot.

## Future

I'm planing on working a little bit more on this game, maybe make the UI a little
prettier. Right now there is no margin for the actual map, so you can't so the
borders.

Maybe a little menu to start the game and a settings menue.

Also to work an the size of the project:

* runing "cargo build" the folder gets to 400+MB
* the binary of the game around 40MB...

For a game that is basically nothing the binary file is pretty big.
