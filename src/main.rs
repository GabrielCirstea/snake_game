/* main.rs
 * A little snake game to learn some Rust*/

use glutin_window::GlutinWindow;
use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::*;
use piston::input::*;
use piston::window::WindowSettings;

use std::collections::LinkedList;
use std::iter::FromIterator;

struct Game {
    gl: GlGraphics,
    snake: Snake,
    rows: u32,
    cols: u32,
    square_width: u32,
    eaten: bool,
    food: Food,
    score: u32,
}

impl Game {
    fn render(&mut self, arg: &RenderArgs) {
        use graphics;

        let WHITE: [f32; 4] = [1.0, 1.0, 1.0, 1.0];
        self.gl.draw(arg.viewport(), |_c, gl| {
            graphics::clear(WHITE, gl);
        });
        self.snake.render(&arg);
        self.food.render(&mut self.gl, arg, self.square_width);
    }

    fn update(&mut self, args: &UpdateArgs) -> bool {
        if !self.snake.update(self.eaten, self.cols, self.rows) {
            return false;
        }

        self.eaten = self.food.update(&self.snake);
        if self.eaten {
            self.score += 1;
            use rand::thread_rng;
            use rand::Rng;
            let mut r = thread_rng();
            loop {
                let x = r.gen_range(1, self.cols);
                let y = r.gen_range(1, self.rows);
                if !self.snake.is_collide(x, y) {
                    self.food = Food { x: x, y: y };
                    break;
                }
            }
        }

        return true;
    }

    fn pressed(&mut self, btn: &Button) {
        let last_dir = self.snake.d.clone();
        self.snake.d = match btn {
            &Button::Keyboard(Key::Up) if last_dir != Direction::DOWN => Direction::UP,
            &Button::Keyboard(Key::Down) if last_dir != Direction::UP => Direction::DOWN,
            &Button::Keyboard(Key::Left) if last_dir != Direction::RIGHT => Direction::LEFT,
            &Button::Keyboard(Key::Right) if last_dir != Direction::LEFT => Direction::RIGHT,
            _ => last_dir,
        };
    }
}

#[derive(Clone, PartialEq)]
enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT,
}

#[derive(Clone)]
struct Snake_Piece(u32, u32);

struct Snake {
    parts: LinkedList<Snake_Piece>,
    gl: GlGraphics,
    width: u32,
    d: Direction,
}

impl Snake {
    fn render(&mut self, arg: &RenderArgs) {
        use graphics;

        let RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
        let squares: Vec<graphics::types::Rectangle> = self
            .parts
            .iter()
            .map(|p| Snake_Piece(p.0 * self.width, p.1 * self.width))
            .map(|p| graphics::rectangle::square(p.0 as f64, p.1 as f64, self.width as f64))
            .collect();

        self.gl.draw(arg.viewport(), |c, gl| {
            let transform = c.transform;
            squares
                .into_iter()
                .for_each(|square| graphics::rectangle(RED, square, transform, gl));
        });
    }

    fn update(&mut self, eaten: bool, cols: u32, rows: u32) -> bool {
        let mut new_front: Snake_Piece = (*self.parts.front().expect("no parts")).clone();
        match self.d {
            Direction::UP => new_front.1 -= 1,
            Direction::DOWN => new_front.1 += 1,
            Direction::LEFT => new_front.0 -= 1,
            Direction::RIGHT => new_front.0 += 1,
        }

        if new_front.1 < 1 || new_front.1 > rows {
            return false;
        }
        if new_front.0 < 1 || new_front.0 > cols {
            return false;
        }

        if !eaten {
            self.parts.pop_back();
        }

        if self.is_collide(new_front.0, new_front.1) {
            return false;
        }

        self.parts.push_front(new_front);
        return true;
    }

    fn is_collide(&self, x: u32, y: u32) -> bool {
        self.parts.iter().any(|p| p.0 == x && p.1 == y)
    }
}

struct Food {
    x: u32,
    y: u32,
}

impl Food {
    fn update(&mut self, s: &Snake) -> bool {
        // undeva se inverseaza variabilele astea
        let front = s.parts.front().unwrap();
        if self.x == front.0 && self.y == front.1 {
            return true;
        }
        return false;
    }

    fn render(&mut self, gl: &mut GlGraphics, args: &RenderArgs, width: u32) {
        use graphics;

        let BLACK = [0.0, 0.0, 0.0, 1.0];
        let x = self.x * width;
        let y = self.y * width;

        let square = graphics::rectangle::square(x as f64, y as f64, width as f64);

        gl.draw(args.viewport(), |c, gl| {
            graphics::rectangle(BLACK, square, c.transform, gl);
        });
    }
}

fn main() {
    let opengl = OpenGL::V3_2;

    const COLS: u32 = 20;
    const ROWS: u32 = 30;
    const SQUARE_WIDTH: u32 = 20;

    let WIDTH = COLS * SQUARE_WIDTH;
    let HEIGHT = ROWS * SQUARE_WIDTH;

    let mut window: GlutinWindow = WindowSettings::new("Snake", [WIDTH, HEIGHT])
        .opengl(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    let mut game = Game {
        gl: GlGraphics::new(opengl),
        rows: ROWS,
        cols: COLS,
        square_width: SQUARE_WIDTH,
        score: 0,
        food: Food { x: 20, y: 20 },
        eaten: false,
        snake: Snake {
            parts: LinkedList::from_iter((vec![Snake_Piece(COLS / 2, ROWS / 2)]).into_iter()),
            gl: GlGraphics::new(opengl),
            width: SQUARE_WIDTH,
            d: Direction::RIGHT,
        },
    };

    let mut events = Events::new(EventSettings::new()).ups(10);
    while let Some(e) = events.next(&mut window) {
        if let Some(r) = e.render_args() {
            game.render(&r);
        }

        if let Some(u) = e.update_args() {
            game.update(&u);
        }

        if let Some(b) = e.button_args() {
            if b.state == ButtonState::Press {
                game.pressed(&b.button);
            }
        }
    }

    println!("Score: {}", game.score);
}
